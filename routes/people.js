var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function(req, res, next) {
  res.send("<div>" + req.body.name + "</div>")
});

router.post('/a', function(req, res, next) {
  res.send("<div>" + req.body.name + "</div>")
});

module.exports = router;
